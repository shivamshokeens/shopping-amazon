import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { BestSellerPage } from '../header/best-seller/best-seller.page';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {

  public now = new Date();
  //public sub = this.now() -2;

  constructor(
    
    public popoverController: PopoverController
  ) {
    console.log(this.now);
    //  console.log("subtract "+this.sub);
  }

  ngOnInit() {
  }
  slideOpts: any = {
    initialSlide: 0,
    speed: 500,
    autoplay: true,
    // centeredSlides: true
    // pagination: {
    //   el: '.swiper-pagination',
    //   type: 'fraction',
    // } by default it is bullets above one eg 1/5 url=https://ionicframework.com/docs/v3/api/components/slides/Slides/
  };

  trainer: any = {
    id: 1,
    name: "shivam"
  }

  productdetails: any = {
    images: [
      "https://image.shutterstock.com/image-photo/colorful-flower-on-dark-tropical-260nw-721703848.jpg",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTruQFbZF0zvcSBo7yTQUpYmy_i33RIxscB2U_sUEnUwrbJmIRP&s",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTf80NS-3wrzh7TWYnGr5OwFDUFMwy3pkofGbaiZwN71jGjbtWy&s",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUQqmCXPSVP98ptqxzUzWyohJGe9FKqAczY9hlNxqwknCqu1Sr&s",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2C_Kyju12Ow0xuapHQJMEGPbitiYZRalrAjIRRZBc6PP_f31m&s"
    ],
    title: "Realme X2 (Pearl White, 64 GB)  (4 GB RAM)",
    discount: "₹1000",
    price: 16999,
  }
 // additionofproductprice= this.productdetails.price + this.productdetails.price; 
  comparision: any = {
    images: [
      {
        id: 1,
        name: "flower",
        imagel: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTf80NS-3wrzh7TWYnGr5OwFDUFMwy3pkofGbaiZwN71jGjbtWy&s",
      },
      {
        id: 2,
        name: "gril",
        imagel: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTruQFbZF0zvcSBo7yTQUpYmy_i33RIxscB2U_sUEnUwrbJmIRP&s",
      },
    ]
  }

  async bestseller(ev: any) {
    const popover = await this.popoverController.create({
      component: BestSellerPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }


}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'left-menu',
    loadChildren: () => import('./left-menu/left-menu.module').then( m => m.LeftMenuPageModule)
  },
  {
    path: 'right-menu',
    loadChildren: () => import('./right-menu/right-menu.module').then( m => m.RightMenuPageModule)
  },
  {
    path: 'product-detail',
    loadChildren: () => import('./product-detail/product-detail.module').then( m => m.ProductDetailPageModule)
  },  {
    path: 'best-seller',
    loadChildren: () => import('./header/best-seller/best-seller.module').then( m => m.BestSellerPageModule)
  },
  {
    path: 'gift-ideas',
    loadChildren: () => import('./header/gift-ideas/gift-ideas.module').then( m => m.GiftIdeasPageModule)
  },


 ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

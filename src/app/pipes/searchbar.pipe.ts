import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchbar',
  pure: true
})
export class SearchbarPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let arg: string = args[0];
    if (arg === undefined || arg === null || arg === "")
      return value;

    return value.filter((x: any) => {

      let name: string = x.title
      return name.toLowerCase().includes(arg.toLowerCase())

    });

  }


}

import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SearchbarPipe } from './searchbar.pipe';

@NgModule({
  imports: [
    IonicModule
  ],
  declarations: [SearchbarPipe],
  exports: [SearchbarPipe]
})
export class PipesModule { }
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PipesModule } from './pipes/pipes.module';
import { LeftMenuPageModule } from './left-menu/left-menu.module';
import { RightMenuPageModule } from './right-menu/right-menu.module';
import { BestSellerPageModule } from './header/best-seller/best-seller.module';
import { BestSellerPage } from './header/best-seller/best-seller.page';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [BestSellerPage], 
  imports: [
      BrowserModule,
      IonicModule.forRoot(),
      AppRoutingModule,
      PipesModule,
      LeftMenuPageModule,
      RightMenuPageModule,
      BestSellerPageModule
    ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

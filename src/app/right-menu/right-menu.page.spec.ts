import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RightMenuPage } from './right-menu.page';

describe('RightMenuPage', () => {
  let component: RightMenuPage;
  let fixture: ComponentFixture<RightMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RightMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

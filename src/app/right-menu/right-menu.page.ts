import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-right-menu',
  templateUrl: './right-menu.page.html',
  styleUrls: ['./right-menu.page.scss'],
})
export class RightMenuPage implements OnInit {

  constructor(
    private menu: MenuController,
  ) { }

  ngOnInit() {
  }

  openEnd() {
      this.menu.open('end');
    }
  

}

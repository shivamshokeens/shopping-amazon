import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RightMenuPageRoutingModule } from './right-menu-routing.module';

import { RightMenuPage } from './right-menu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RightMenuPageRoutingModule
  ],
  declarations: [RightMenuPage],
  exports: [RightMenuPage]
})
export class RightMenuPageModule {}

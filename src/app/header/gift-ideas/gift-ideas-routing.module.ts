import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GiftIdeasPage } from './gift-ideas.page';

const routes: Routes = [
  {
    path: '',
    component: GiftIdeasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GiftIdeasPageRoutingModule {}

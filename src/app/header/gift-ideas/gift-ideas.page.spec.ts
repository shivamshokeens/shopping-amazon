import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GiftIdeasPage } from './gift-ideas.page';

describe('GiftIdeasPage', () => {
  let component: GiftIdeasPage;
  let fixture: ComponentFixture<GiftIdeasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftIdeasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GiftIdeasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

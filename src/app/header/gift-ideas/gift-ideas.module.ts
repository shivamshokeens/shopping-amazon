import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GiftIdeasPageRoutingModule } from './gift-ideas-routing.module';

import { GiftIdeasPage } from './gift-ideas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GiftIdeasPageRoutingModule
  ],
  declarations: [GiftIdeasPage],
  exports: [GiftIdeasPage]
})
export class GiftIdeasPageModule {}

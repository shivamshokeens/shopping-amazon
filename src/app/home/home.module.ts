import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { PipesModule } from '../pipes/pipes.module';
import { LeftMenuPageModule } from '../left-menu/left-menu.module';
import { RightMenuPageModule } from '../right-menu/right-menu.module';

@NgModule({
  imports: [
    //LeftMenuPageModule,
  //  RightMenuPageModule,
    PipesModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}

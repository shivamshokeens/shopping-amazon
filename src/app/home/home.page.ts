import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, PopoverController } from '@ionic/angular';
import { RightMenuPage } from '../right-menu/right-menu.page';
import { LeftMenuPage } from '../left-menu/left-menu.page';
import { Router } from '@angular/router';
import { BestSellerPage } from '../header/best-seller/best-seller.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public searcha: any;

  constructor(
    private menu: MenuController,    
    public modalController: ModalController,
    private router: Router,
    public popoverController: PopoverController
  ) { }
  ngOnInit() {
  }
  public items: Array<{ title: string; pimage: string }> = [
    {
      title: "Blue Flower",
      pimage: "https://images.pexels.com/photos/326055/pexels-photo-326055.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      title: "Red Flower",
      pimage: "https://images.pexels.com/photos/326055/pexels-photo-326055.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    }
    ,
    {
      title: "White Flower",
      pimage: "https://images.pexels.com/photos/326055/pexels-photo-326055.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      title: " yellow Flower",
      pimage: "https://images.pexels.com/photos/326055/pexels-photo-326055.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      title: "blueberry Flower",
      pimage: "https://images.pexels.com/photos/326055/pexels-photo-326055.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    }
  ]
  async leftmenu() {
    const modal = await this.modalController.create({
      component: LeftMenuPage,
      cssClass: 'modal-heigth'
    });
    return await modal.present();
  }

  async rightmenu() {
    const modal = await this.modalController.create({
      component: RightMenuPage,
      cssClass: 'modal-heigth'
    });
    return await modal.present();
  }

  plist(){
    this.router.navigateByUrl('/product-detail');
  }

  async bestseller(ev: any) {
    const popover = await this.popoverController.create({
      component: BestSellerPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

}

import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.page.html',
  styleUrls: ['./left-menu.page.scss'],
})
export class LeftMenuPage implements OnInit {

  constructor(
    private menu: MenuController,
  ) { }

  ngOnInit() {
  }
  openFirst() {
      this.menu.enable(true, 'first');
      this.menu.open('first');
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LeftMenuPage } from './left-menu.page';

describe('LeftMenuPage', () => {
  let component: LeftMenuPage;
  let fixture: ComponentFixture<LeftMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LeftMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
